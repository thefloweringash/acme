require 'openssl'

def root?(cert)
  cert.subject == cert.issuer
end

def trust_chain(cert_bag, cert)
  return enum_for(:trust_chain, cert_bag, cert) unless block_given?
  loop do
    issuer = cert_bag[cert.issuer]
    yield issuer
    raise "Missing cert for #{cert.issuer}" unless issuer
    break if root?(issuer)
    cert = issuer
  end
end

class Dumper
  def initialize(domain)
    @domain = domain
  end

  def method_missing(name, *certs)
    puts name
    certs.each do |c|
      puts "\t#{c.subject.to_s}"
    end
    File.write("#{@domain}-#{name.to_s}.pem",
               certs.map(&:to_pem).join)
  end

  def self.dump(domain, &block)
    Dumper.new(domain).instance_eval(&block)
  end
end

cert_bag = Dir['./certs/*'].map do |cert_filename|
  OpenSSL::X509::Certificate.new(File.read(cert_filename))
end.each_with_object({}) do |cert, bag|
  bag[cert.subject] = cert
end

domain = ARGV[0]
target = OpenSSL::X509::Certificate.new(File.read("#{domain}.crt"))

chain = trust_chain(cert_bag, target).to_a
intermediates = chain.dup
root = intermediates.pop

Dumper.dump(domain) do
  root root
  intermediates(*intermediates)
  trustchain *chain
  chain      target, *intermediates
  fullchain  target, *chain
end
