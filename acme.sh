#!/bin/bash

cd "$(dirname "$0")"
acme_dir="./challenges"
cert_dir="./certs"

die() {
    echo "$@"
    exit 1
}

mkdir -p ${acme_dir} ${cert_dir}

domains=$(hostname -f)

if [ ! -f account.key ]; then
    openssl genrsa 4096 > account.key
fi

for dom in $domains; do
    if [ ! -f ${dom}.key ]; then
        save_umask=$(umask)
        umask 022
        openssl genrsa 4096 > ${dom}.key
        umask ${save_umask}
    fi

    if [ ! -f ${dom}.csr ]; then
        openssl req -new -sha256 -key ${dom}.key -subj "/CN=${dom}" > ${dom}.csr
    fi

    acme-tiny \
        --account-key account.key \
        --csr ${dom}.csr \
        --acme-dir ${acme_dir} \
        > ${dom}.crt \
        || die 'Failed to create certificate'

    ruby gen_chain.rb ${dom}
done

